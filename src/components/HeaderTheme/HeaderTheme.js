import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div className="h-24 w-full pb-5 flex items-center justify-between shadow-lg px-20">
      <NavLink to="/">
        <img
          style={{ height: "50px", marginLeft: "50px" }}
          src="https://s2.coinmarketcap.com/static/img/coins/200x200/3429.png"
          alt=""
        />
        <p className="text-black-500 text-3xl font-medium">CyberMovie</p>
      </NavLink>

      <div className="space-x-5 text-lg font-medium text-black-500">
        <span>Tin tức</span>
        <span>Cụm rạp</span>
        <span>Lịch chiếu</span>
      </div>

      <UserNav />
    </div>
  );
}
