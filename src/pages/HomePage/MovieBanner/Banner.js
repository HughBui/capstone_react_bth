import { Carousel } from "antd";
import React from "react";
const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

const Banner = () => {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };

  return (
    <Carousel
      afterChange={onChange}
      style={{
        width: "100%",
        height: "600px",
        // backgroundColor: "red",
        // backgroundImage: `url("./img/12.jpg")`,
      }}
    >
      <div>
        <img
          style={{ width: "1400px", height: "650px" }}
          src="./img/1.jpg"
        ></img>
        {/* <h3 style={contentStyle}>1</h3> */}
      </div>
      <div>
        <img
          style={{ width: "1400px", height: "650px" }}
          src="./img/2.jpg"
        ></img>
        {/* <h3 style={contentStyle}>2</h3> */}
      </div>
      <div>
        <img
          style={{ width: "1400px", height: "650px" }}
          src="./img/3.jpg"
        ></img>
        {/* <h3 style={contentStyle}>3</h3> */}
      </div>
      {/* <div>
        <img
          style={{ width: "1400px", height: "650px" }}
          src="./img/4.jpg"
        ></img>
      </div> */}
    </Carousel>
  );
};

export default Banner;
