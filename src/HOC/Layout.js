import React from "react";
import HeaderTheme from "../components/HeaderTheme/HeaderTheme";

export function LayoutTheme({ Component }) {
  return (
    <div
      style={{
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <HeaderTheme />
      <div className="flex-grow">
        <Component />
      </div>
      <footer
        style={{
          textAlign: "center",
          color: "black",
          fontWeight: "bold",
          fontSize: "20px",
        }}
        className="mt-5 bg-slate-500 h-20 w-full"
      >
        <p>Công ty cổ phần CyberCinema</p>
        <div
          className="m-5, mb-5"
          style={{ fontSize: "30px", color: "white", textAlign: "center" }}
        >
          <i class="fab fa-facebook"></i>
          <i class="fab fa-twitter ml-5 mr-5"></i>
          <i class="fab fa-instagram"></i>
        </div>
      </footer>
    </div>
  );
}
